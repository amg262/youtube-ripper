#!/usr/bin/env python

import argparse
import logging.config
import threading
from collections import Counter

import config

if __name__ == '__main__':
    # parse command line arguments
    parser = argparse.ArgumentParser(description='FEs')
    parser.add_argument('url', type=str, help='URL of youtube resrouce')
    parser.add_argument('-v', '--video', default=config.video, help="video (default: " + str(config.video) + ")")
    parser.add_argument('-g', '--group', default=config.group, help="group (default: " + str(config.group) + ")")
    parser.add_argument('-u', '--user', default=config.user, help="video (user: " + str(config.user) + ")")
    parser.add_argument('-o', '--owner', default=config.owner, help="owner (owner: " + str(config.owner) + ")")
    parser.add_argument('-c', '--channel', default=config.channel,
                        help="channel (default: " + str(config.channel) + ")")
    parser.add_argument('-p', '--playlist', default=config.playlist,
                        help="playlist (default: " + str(config.playlist) + ")")
    parser.add_argument('-f', '--feed', default=config.feed, help="feed (default: " + str(config.feed) + ")")
    parser.add_argument('-l', '--list', default=config.list, help="list (default: " + str(config.list) + ")")
    parser.add_argument('-s', '--set', default=config.set, help="set (default: " + str(config.set) + ")")
parser.add_argument('-t', '--threads', default=config.threads, help="threads (default: " + str(config.threads) + ")")
parser.add_argument('-w', '--wordlist', default=config.wordlist, help="wordlist (default: " + str(config.wordlist) + ")")
    
args = parser.parse_args()
config.url = args.url
config.video = args.video
config.group = args.group
config.user = args.user
config.channel = args.channel
config.playlist = args.playlist
config.feed = args.feed
config.list = args.list
config.set = args.set
# logger configuration
logging.config.fileConfig("ytr.conf")
logger = logging.getLogger("amg")

logger.info("Target URL: %s", config.url)  # check URL and user (if user not set, enumerate usernames)
logger.info("Checking URL & username...")

usernames = []
if config.user:
    usernames.append(config.user)

logger.info("Users: %s", usernames)  # check URL and user (if user not set, enumerate usernames)

# load login check tasks into queue
logger.debug("Loading wordlist...")

file = open(config.wordlist, "r")

wordlist = file.readlines()

count = Counter(wordlist)

for line in wordlist:
    print(line)


def worker():
    """thread worker function"""
    print('Worker')
    return


threads = []
for i in range(5):
    t = threading.Thread(target=worker)
    threads.append(t)
    t.start()
