#
# wpbf config file
#
# Copyright 2011 Andres Tarantini (atarantini@gmail.com)
#
# This file is part of wpbf.
#
# wpbf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wpbf is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with wpbf.  If not, see <http://www.gnu.org/licenses/>.
#

url = "https://www.youtube.com/watch?v=UIah_cVBxbo&t=164s"
video = url
group = 1
user = "https://www.youtube.com/user/MarkDice"
owner = ""
owner_elem = "yt-simple-endpoint style-scope ytd-video-owner-renderer"
owner_anchor_class = "yt-simple-endpoint.style-scope.ytd-video-owner-renderer"
owner_anchor_str = ""
channel = ""
playlist = ""
feed = ""
list = ""
set = ""

wordlist = "wordlist.txt"
threads = 5
